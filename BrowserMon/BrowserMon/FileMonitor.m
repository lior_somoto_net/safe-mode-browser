//
//  FileMonitor.m
//  BrowserMon
//
//  Created by Alisa on 27/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "FileMonitor.h"
#import "WindowsUtils.h"
#import "CDLZ4.h"
#import "Dejsonlz4.h"
#import "JSONKit.h"

@implementation FileMonitor

- (instancetype)initWithPath:(NSString*)path {
	self = [[FileMonitor alloc] init];
	
	if (self) {
		_path = path;
	}
	return self;
}

- (void)watchFile {	
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	int fildes = open([_path UTF8String], O_EVTONLY);
	__block dispatch_source_t source = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE,fildes,
															  DISPATCH_VNODE_DELETE | DISPATCH_VNODE_WRITE | DISPATCH_VNODE_EXTEND | DISPATCH_VNODE_ATTRIB | DISPATCH_VNODE_LINK | DISPATCH_VNODE_RENAME | DISPATCH_VNODE_REVOKE,
															  queue);
	
	dispatch_source_set_event_handler(source, ^(void) {
		//Get the flags describing the change:
		unsigned long flags = dispatch_source_get_data(source);
		
		if (flags & DISPATCH_VNODE_DELETE) {	// The file was deleted (in some cases a monitored config file is deleted and created again instead of being overwritten)?
			
			[NSThread sleepForTimeInterval:1];
			dispatch_source_cancel(source);
			[self watchFile];	// Restart monitoring.
		}
										  
		// Reload config file:
		if ([self.path rangeOfString:@"/Safari/"].location != NSNotFound) {
			[self handleSafari];
		}
		else if ([self.path rangeOfString:@"/Firefox/"].location != NSNotFound) {
			[self handleFirefox];
		}
	});
	dispatch_source_set_cancel_handler(source, ^(void) {
		close(fildes);
	});
	dispatch_resume(source);
}

- (void)handleSafari {
	NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:_path];
	NSArray *wndsFile = [dict objectForKey:@"SessionWindows"];
	NSUInteger numWndsFile = [wndsFile count];
	NSLog(@"Num of Safari windows from session file: %lu", numWndsFile);
	
	NSArray *wndsApi = [WindowsUtils getWindowsForApp:@"Safari" withSort:YES];
	NSUInteger numWndsApi = [wndsApi count];
	NSLog(@"Num of Safari windows from enumeration API: %lu", numWndsApi);
	
	if (numWndsApi > numWndsFile) {
		NSLog(@"Safari Private Window is open");
	}
}

- (void)handleFirefox {
	int a = 3;
	char *inpuOutput[2];
	const char * cname = [_path UTF8String];
	NSString* pathToFileNew = [NSString stringWithFormat:@"%@/%@", [_path stringByDeletingLastPathComponent], @"recovery.json"];
	const char * cnameNew = [pathToFileNew UTF8String];
	
	inpuOutput[0] = (char*)cname;
	inpuOutput[1] = (char*)cnameNew;
	startDec(a, inpuOutput);
	
	NSString *fileData = [NSString stringWithContentsOfFile:pathToFileNew encoding:NSUTF8StringEncoding error:nil];
	NSDictionary *currentDictionary = [fileData objectFromJSONString];
	NSArray *wndsFile = [currentDictionary objectForKey:@"windows"];
	NSUInteger numWndsFile = [wndsFile count];
	NSLog(@"Num of Firefox windows from file: %lu", numWndsFile);
	
	NSArray *wndsApi = [WindowsUtils getWindowsForApp:@"Firefox" withSort:YES];
	NSUInteger numWndsApi = [wndsApi count];
	NSLog(@"Num of Firefox windows from enumeration API: %lu", numWndsApi);
	
	if (numWndsApi > numWndsFile) {
		NSLog(@"Firefox Private Window is open");
	}
}

@end
