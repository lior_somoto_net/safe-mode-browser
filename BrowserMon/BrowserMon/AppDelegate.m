//
//  AppDelegate.m
//  BrowserMon
//
//  Created by Alisa on 13/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "AppDelegate.h"
#import "FileUtils.h"
#import "Executer.h"
#import "UIElementUtilities.h"
#import "FileMonitor.h"
#import "ProcessMonitor.h"

static NSArray *_browserNames = nil;

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate {
	AXUIElementRef	_systemWideElement;
	NSPoint			_lastMousePoint;
	AXUIElementRef	_currentUIElement;
}

- (void)dealloc {
	if (_systemWideElement) {
		CFRelease(_systemWideElement);
	}
	if (_currentUIElement) {
		CFRelease(_currentUIElement);
	}
	[super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	_browserNames = @[ @"Safari", @"Firefox", @"Google Chrome" ];
	_browserNames = [_browserNames retain];
	
	// We first have to check if the Accessibility APIs are turned on.  If not, we have to tell the user to do it (they'll need to authenticate to do it).  If you are an accessibility app (i.e., if you are getting info about UI elements in other apps), the APIs won't work unless the APIs are turned on.
	if (!AXAPIEnabled()) {
		
		NSAlert *alert = [[[NSAlert alloc] init] autorelease];
		
		[alert setAlertStyle:NSWarningAlertStyle];
		[alert setMessageText:@"Browser Mon requires that the Accessibility API be enabled."];
		[alert setInformativeText:@"Would you like to launch System Preferences so that you can turn on \"Enable access for assistive devices\"?"];
		[alert addButtonWithTitle:@"Open System Preferences"];
		[alert addButtonWithTitle:@"Continue Anyway"];
		[alert addButtonWithTitle:@"Quit Browser Mon"];
		
		NSInteger alertResult = [alert runModal];
		
		switch (alertResult) {
			case NSAlertFirstButtonReturn: {
				NSString* urlString = @"x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility";
				[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString:urlString]];
			}
				break;
				
			case NSAlertSecondButtonReturn: // just continue
			default:
				break;
				
			case NSAlertThirdButtonReturn:
				[NSApp terminate:self];
				return;
				break;
		}
	}
	
	// Register to a notification about an application launch:
	[[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(appDidLaunch:) name:NSWorkspaceDidLaunchApplicationNotification object:nil];
	
	// Start monitoring the LastSession.plist file used by Safari:
	NSString *libraryDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *sfSessionFile = [NSString stringWithFormat:@"%@/Safari/LastSession.plist", libraryDir];
	
	NSLog(@"Starting monitoring file %@", sfSessionFile);
	FileMonitor *sfMon = [[[FileMonitor alloc] initWithPath:sfSessionFile] autorelease];
	[sfMon watchFile];
	
	NSString *ffProfilesDir = [FileUtils calcDefaultFFProfilePath];
	if ([ffProfilesDir length] > 0) {	// Precaution - make sure Firefox is installed on this machine
		NSString *ffWatchPath = [NSString stringWithFormat:@"%@/sessionstore-backups/recovery.jsonlz4", ffProfilesDir];
		
		// Create an empty file at the FF watch path if it doesn't exist
		if (![[NSFileManager defaultManager] fileExistsAtPath:ffWatchPath]) {
			NSString *contents = @"";
			[contents writeToFile:ffWatchPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
		}
		
		NSLog(@"Starting monitoring file %@", ffWatchPath);
		FileMonitor *ffMon =  [[[FileMonitor alloc] initWithPath:ffWatchPath] autorelease];
		[ffMon watchFile];
	}
	
	ProcessMonitor *procMon = [[[ProcessMonitor alloc] init] autorelease];
	[procMon monitorProcessWithBundleId:@"com.google.Chrome"];
	
	_systemWideElement = AXUIElementCreateSystemWide();

	// Start monitoring the location of the cursor and check the underlying accessibility element:
	[self performTimerBasedUpdate];
}

// -------------------------------------------------------------------------------
//	performTimerBasedUpdate:
//
//	Timer to continually update the current uiElement being examined.
// -------------------------------------------------------------------------------
- (void)performTimerBasedUpdate
{
	[self updateCurrentUIElement];
	
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(performTimerBasedUpdate) userInfo:nil repeats:NO];
}

// -------------------------------------------------------------------------------
//	updateCurrentUIElement:
// -------------------------------------------------------------------------------
- (void)updateCurrentUIElement
{
	// The current mouse position with origin at top right.
	NSPoint cocoaPoint = [NSEvent mouseLocation];
	
	// Only ask for the UIElement under the mouse if has moved since the last check.
	if (!NSEqualPoints(cocoaPoint, _lastMousePoint)) {
		
		CGPoint pointAsCGPoint = [UIElementUtilities carbonScreenPointFromCocoaScreenPoint:cocoaPoint];
		AXUIElementRef newElement = NULL;
		
		// Ask Accessibility API for UI Element under the mouse
		// And update the display if a different UIElement
		if (AXUIElementCopyElementAtPosition(_systemWideElement, pointAsCGPoint.x, pointAsCGPoint.y, &newElement) == kAXErrorSuccess &&
			newElement &&
			(_currentUIElement == NULL || ! CFEqual(_currentUIElement, newElement))) {
			[self setCurrentUIElement:newElement];
			[self checkBrowserElement];
		}
		
		_lastMousePoint = cocoaPoint;
	}
}

- (void)checkBrowserElement {
	NSString *elemRole = [UIElementUtilities roleOfUIElement:_currentUIElement];
	
	if ([elemRole isEqualToString:@"AXDockItem"]) {
		NSString *elemTitle = [UIElementUtilities titleOfUIElement:_currentUIElement];
		
		if ([_browserNames indexOfObject:elemTitle] != NSNotFound) {
			NSLog(@"Detected hover over %@ dock icon", elemTitle);
//			[self displayALert:[NSString stringWithFormat:@"Detected hover over %@ dock icon", elemTitle]];
		}
	}
	else if ([elemRole isEqualToString:@"AXTextField"]) {
		// Check the parent of the current element:
		AXUIElementRef parent = [UIElementUtilities parentOfUIElement:_currentUIElement];
		NSString *parentRole = [UIElementUtilities roleOfUIElement:parent];
		
		// This is the Safari omnibar?
		if ([parentRole isEqualToString:@"AXSafariAddressAndSearchField"]) {
			BOOL isFocused = [self isCurrElemFocused];
			
			if (isFocused) {
				NSLog(@"Safari omnibar is focused.");
			}
		}
		else if ([parentRole isEqualToString:@"AXToolbar"]) {
			// Extract the application name from the topmost ancestor:
			NSArray *lineage = [UIElementUtilities lineageOfUIElement:parent];
			NSString *topAncestorDesc = [lineage objectAtIndex:0];
			NSUInteger quoteStart = [topAncestorDesc rangeOfString:@"\u201c"].location;
			NSUInteger quoteEnd = [topAncestorDesc rangeOfString:@"\u201d"].location;
			NSString *containingApp = [topAncestorDesc substringWithRange:NSMakeRange(quoteStart + 1, quoteEnd - quoteStart - 1)];
			
			if ([containingApp containsString:@"Chrome"]) {	// We're hovering above the Chrome omnibar?
				BOOL isFocused = [self isCurrElemFocused];
				
				if (isFocused) {
					NSLog(@"Chrome omnibar is focused.");
				}
			}
		}
	}
}

- (BOOL)isCurrElemFocused {
	CFBooleanRef isFocused = (CFBooleanRef)[UIElementUtilities valueOfAttribute:@"AXFocused" ofUIElement:_currentUIElement];
	Boolean boolFocus = CFBooleanGetValue(isFocused);
	return boolFocus;
}

- (void)displayALert:(NSString *)message {
	NSAlert *alert = [[[NSAlert alloc] init] autorelease];
	
	[alert addButtonWithTitle:@"OK"];
	[alert addButtonWithTitle:@"Abort"];
	[alert setMessageText:message];
	[alert setAlertStyle:NSAlertStyleWarning];
	if ([alert runModal] == NSAlertSecondButtonReturn) {	// User decided to abort our process
		[NSApp terminate:0];
	}
}

// -------------------------------------------------------------------------------
//	setCurrentUIElement:uiElement
// -------------------------------------------------------------------------------
- (void)setCurrentUIElement:(AXUIElementRef)uiElement
{
	[(id)_currentUIElement autorelease];
	_currentUIElement = (AXUIElementRef)[(id)uiElement retain];
}

- (void)appDidLaunch:(NSNotification*)note
{
	NSString *launchAppName = [[note userInfo] objectForKey:@"NSApplicationName"];
	
	if ([_browserNames indexOfObject:launchAppName] != NSNotFound) {
		NSLog(@"Browser %@ was launched", launchAppName);
//		[self displayALert:[NSString stringWithFormat:@"Browser %@ was launched", launchAppName]];
	}
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}

@end
