//
//  main.m
//  BrowserMon
//
//  Created by Alisa on 13/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	NSUserDefaults *args = [NSUserDefaults standardUserDefaults];
	NSString *trigger = [args stringForKey:@"trigger"];
	
	if ([trigger length] == 0) {
		[args setObject:@"install" forKey:@"trigger"];
	}
	[args setObject:NSProcessInfo.processInfo.arguments[0] forKey:@"runPath"];
	
	return NSApplicationMain(argc, argv);
}
