//
//  FileUtils.m
//  LaunchMon
//
//  Created by Alisa on 12/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "FileUtils.h"

@implementation FileUtils

/*! \fn + (BOOL)createDirectory:(NSString *)fullPath
 *  \brief Creates a directory at the specified full path.
 *  \param fullPath - full path of the directory we want to create.
 *  \return YES in case directory was created successfully, NO otherwise.
 */
+ (BOOL)createDirectory:(NSString *)fullPath withIntermediateDirs:(BOOL)withIntermediateDirs
{
	NSLog(@"Creating directory %@", fullPath);
	
	NSError *error = nil;
	BOOL ok = [[NSFileManager defaultManager] createDirectoryAtPath:fullPath withIntermediateDirectories:withIntermediateDirs attributes:nil error:&error];
	
	if (!ok) {	// Failed to create directory?
		NSLog(@"Failed to create directory %@, error: %@", fullPath, [error localizedDescription]);
	}
	return ok;
}

/*! \fn + (BOOL)saveData:(NSData *)contents toPath:(NSString *)path atomically:(BOOL)atomically
 *  \brief Save binary data into a file.
 *  \param contents - an NSData object storing the content to be saved
 *  \param path - the destination path
 *	\param atomically - YES to use an auxillary file and rename it to <path>, NO to write directly to <path>.
 *  \return YES in case of successful operation completion, NO in case an error occurred.
 */
+ (BOOL)saveData:(NSData *)contents toPath:(NSString *)path atomically:(BOOL)atomically {
	if ([path length] == 0) {	// We received nil or an empty string as the input file path?
		NSLog(@"Invalid argument: dest file path was not specified");
		return NO;	// Return an error indication.
	}
	if (contents == nil) {	// We received nil as the content to be saved?
		NSLog(@"Invalid argument: buffer to be saved was not specified");
		return NO;	// Return an error indication.
	}
	
	// Set the 'atomic' flag in case atomically is YES
	NSDataWritingOptions options = (atomically ? NSDataWritingAtomic : (NSDataWritingOptions)0);
	NSError *error = nil;
	BOOL result = [contents writeToFile:path options:options error:&error];
	
	if (!result) {	// Failed to save file?
		NSLog(@"Failed to write to file %@. Error: %@", path, [error localizedDescription]);
		return NO;
	}
	NSLog(@"Saved file %@ successfully", path);
	return result;
}

+ (NSString *)calcDefaultFFProfilePath {
	NSString *appSupportDir = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	
	// Check if Firefox is installed for this user:
	NSString *ffDirectory = [appSupportDir stringByAppendingPathComponent:@"Firefox"];
	BOOL isDirectory = NO;
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:ffDirectory isDirectory:&isDirectory] ||
		!isDirectory) {
		return nil;
	}
	
	NSString *profilesDir = [ffDirectory stringByAppendingPathComponent:@"Profiles"];
	NSArray * files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:profilesDir error:nil];
	for (NSString *profile in files) {
		if ([profile rangeOfString:@".default"].location != NSNotFound) {
			return [profilesDir stringByAppendingPathComponent:profile];
		}
	}
	return nil;
}

@end
