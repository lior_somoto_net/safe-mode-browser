//
//  Executer.m
//  LaunchMon
//
//  Created by Alisa on 12/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "Executer.h"

@implementation Executer

/*! \fn + (BOOL)runWithArgsAndIOPipes:(NSString*)path args:(NSArray*)args wait:(BOOL)aWait inputData:(NSString*)data outputData:(NSString**)outputData
 *  \brief Runs an executable with the specified args with the option to provide input to the app and to read its output.
 *  \param path - a string storing the path to an executable file.
 *	\param args - array of args to be passed to the executable.
 *	\param aWait - YES if we want to wait for the execution completion, NO otherwise.
 *	\param inputData - a string with input data to be passed to the app - optional input param, pass nil or an empty string if this option does not interest you.
 *	\param outputData - a string storing the execution output (this option is relevant only if aWait == YES) - optional output param, pass nil if this option does not interest you.
 *  \return YES in of successful execution, NO otherwise.
 */
+ (BOOL)runWithArgsAndIOPipes:(NSString*)path
						 args:(NSArray*)args
						 wait:(BOOL)aWait
					inputData:(NSString*)inputData
				   outputData:(NSString**)outputData
{
	NSTask *task = [[NSTask alloc] init];
	BOOL ok = FALSE;
	
	[task setLaunchPath:path];
	
	if (args != nil) {	// Command line arguments were specified?
		[task setArguments:args];
	}
	
	if (outputData != nil) {	// We're interested in the output?
		[task setStandardOutput:[NSPipe pipe]];
		[task setStandardError:[NSPipe pipe]];
	}
	
	@try	// NSTask has been known to throw
	{
		NSPipe* inputPipe = nil;
		
		if ([inputData length] > 0) {	// Input to the application was specified?
			inputPipe = [[NSPipe alloc] init];	// Create an input pipe
			[task setStandardInput:inputPipe];
		}
		
		[task launch];
		
		if (inputPipe != nil) {	// An input pipe was created for this task?
			// Write the data that was passed as the input for the command line execution:
			[[inputPipe fileHandleForWriting] writeData:(NSData *)[inputData dataUsingEncoding:NSUTF8StringEncoding]];
			[[inputPipe fileHandleForWriting] closeFile];
		}
		
		if (aWait) {	// We want to wait for the execution completion?
			[task waitUntilExit];
			ok = ([task terminationStatus] == 0);
			
			if (outputData != nil) {	// We're interested in the output? (no matter if we succeeded or failed)
				*outputData = @"";	// Set the outpu to an empty string at first.
				
				// Read data from stdout:
				NSData *dataStdout = [[[task standardOutput] fileHandleForReading] readDataToEndOfFile];
				
				if ([dataStdout length]) {	// There is available data in the task's stdout?
					// Append the stdout data to the previously assigned empty string.
					*outputData = [NSString stringWithFormat:@"%@%@", *outputData, [[NSString alloc] initWithData:dataStdout encoding:NSUTF8StringEncoding]];
				}
				
				// Try to read from the tasks' stderr:
				NSData *dataStderr = [[[task standardError] fileHandleForReading] readDataToEndOfFile];
				
				if ([dataStderr length]) {	// We read data from stderr?
					// Append the stderr output to the previously read data.
					*outputData = [NSString stringWithFormat:@"%@%@", *outputData, [[NSString alloc] initWithData:dataStderr encoding:NSUTF8StringEncoding]];
				}
			}
		}
		else {	// We execute the task asynchronously?
			ok = YES;	// Assume that we succeeded.
		}
		
	}
	@catch (NSException *ex) {
		NSLog(@"Caught %@ exception while running taskReason: %@", [ex name], [ex reason]);
	}
	return ok;
}

@end
