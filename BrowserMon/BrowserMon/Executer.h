//
//  Executer.h
//  LaunchMon
//
//  Created by Alisa on 12/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Executer : NSObject

+ (BOOL)runWithArgsAndIOPipes:(NSString*)path
						 args:(NSArray*)args
						 wait:(BOOL)aWait
					inputData:(NSString*)inputData
				   outputData:(NSString**)outputData;

@end
