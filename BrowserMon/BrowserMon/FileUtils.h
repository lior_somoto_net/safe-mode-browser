//
//  FileUtils.h
//  LaunchMon
//
//  Created by Alisa on 12/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject

+ (BOOL)createDirectory:(NSString *)fullPath withIntermediateDirs:(BOOL)withIntermediateDirs;
+ (BOOL)saveData:(NSData *)contents toPath:(NSString *)path atomically:(BOOL)atomically;

+ (NSString *)calcDefaultFFProfilePath;

@end
