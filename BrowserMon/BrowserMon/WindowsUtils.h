//
//  WindowsUtils.h
//  BrowserMon
//
//  Created by Alisa on 27/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WindowsUtils : NSObject

+ (NSArray *)getWindowsForApp:(NSString*)appName withSort:(BOOL)sortByWinOrder;

@end
