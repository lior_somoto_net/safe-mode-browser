//
//  WindowsUtils.m
//  BrowserMon
//
//  Created by Alisa on 27/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "WindowsUtils.h"

@implementation WindowsUtils

/*!	\fn + (NSArray *)getWindowsForApp:(NSString*)appName listAll:(BOOL)listAll withSort:(BOOL)sortByWinOrder windRefId:(int)windRefId
 *	\brief Returns an array of all App (for example: Google Chrome) windows (optionally sorted by their order).
 *  \param appName - Appname we want to get all of its windows.
 *	\param sortByWinOrder - YES to sort windows by their order, NO otherwise.
 *	\return Array of dictionaries storing the properties of the windows belonging to the specified process.
 */
+ (NSArray *)getWindowsForApp:(NSString*)appName
					 withSort:(BOOL)sortByWinOrder {
	unsigned windlistenumtype = kCGWindowListOptionOnScreenOnly | kCGWindowListExcludeDesktopElements;
	CGWindowListOption listOptions = windlistenumtype;
	NSArray*     windowList = (__bridge NSArray*)CGWindowListCopyWindowInfo(listOptions, kCGNullWindowID);
	NSPredicate* filterByOwner = [NSPredicate predicateWithFormat:@"kCGWindowOwnerName LIKE[c] %@", appName];
	
	NSArray* arr = [windowList filteredArrayUsingPredicate:filterByOwner];
	
	if (sortByWinOrder) {	// We want to sort the results by the order of the windows?
		// Create a sort descriptor array. It consists of a single descriptor that sorts based on the kWindowOrderKey in ascending order
		NSSortDescriptor *sortdesc = [[NSSortDescriptor alloc] initWithKey:@"windowOrder" ascending:YES];
		NSArray *   sortDescriptors = @[ sortdesc ];
		
		// Next sort the selection based on that sort descriptor array
		NSArray*    sortedSelection = [arr sortedArrayUsingDescriptors:sortDescriptors];
		return sortedSelection;
	}
	
	return arr;
}

@end
