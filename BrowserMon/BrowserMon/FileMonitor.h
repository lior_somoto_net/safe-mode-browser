//
//  FileMonitor.h
//  BrowserMon
//
//  Created by Alisa on 27/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileMonitor : NSObject

- (instancetype)initWithPath:(NSString*)path;
- (void)watchFile;

@property (retain) NSString *path;

@end
