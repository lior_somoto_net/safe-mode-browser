//
//  ProcessMonitor.m
//  BrowserMon
//
//  Created by Alisa on 27/06/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

#import "ProcessMonitor.h"
#import "Executer.h"
#import <Cocoa/Cocoa.h>

@interface ProcessMonitor()

@property (retain) NSArray *childProcs;

@end

@implementation ProcessMonitor

- (void)monitorProcessWithBundleId:(NSString *)bundleId {
	NSArray *array = [NSRunningApplication runningApplicationsWithBundleIdentifier:bundleId];
	if (array == nil || [array count] == 0) {
		return;
	}
	
	pid_t const pid = [[array firstObject] processIdentifier];
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	__block dispatch_source_t source = dispatch_source_create(DISPATCH_SOURCE_TYPE_PROC, pid,
															  DISPATCH_PROC_EXIT | DISPATCH_PROC_FORK | DISPATCH_PROC_EXEC | DISPATCH_PROC_SIGNAL,
															  queue);
	
	_childProcs = [self getChildProcesses:pid];
	dispatch_source_set_event_handler(source, ^(void) {
		[self procEventsHandler:source forProcess:pid];
	});
	dispatch_resume(source);
}

- (NSArray *)getChildProcesses:(pid_t)mainPid {
	NSString *output = nil;
	
	[Executer runWithArgsAndIOPipes:@"/usr/bin/pgrep"
							   args:@[ @"-P", [@(mainPid) stringValue] ]
							   wait:YES
						  inputData:nil
						 outputData:&output];
	output = [output substringToIndex:output.length - 1];
	
	NSArray *procList = [output componentsSeparatedByString:@"\n"];
	return procList;
}

- (void)procEventsHandler:(dispatch_source_t)source forProcess:(pid_t)pid {
	unsigned long flags = dispatch_source_get_data(source);
	
	if (flags & DISPATCH_PROC_EXIT) {
		dispatch_source_cancel(source);
	}
	else {
		NSArray *newChildProcs = [self getChildProcesses:pid];
		if ([[newChildProcs lastObject] integerValue] > [[_childProcs lastObject] integerValue]) {
			NSLog(@"Detected start of new child process");
			
			// Request the value of the "mode" property for the topmost Chrome window: 1 for a normal window, 0 for an incognito window and -1 in case an internal error occurred.
			NSDictionary *error = nil;
			
			NSMutableString *scriptText = [NSMutableString stringWithString:@"tell application \"Google Chrome\"\n"];
			[scriptText appendString:@"	set props to get properties of window 1\n"];
			[scriptText appendString:@"	if mode in props is \"normal\" then\n"];
			[scriptText appendString:@"		set window_mode to 1\n"];
			[scriptText appendString:@"	else if mode in props is \"incognito\" then\n"];
			[scriptText appendString:@"		set window_mode to 0\n"];
			[scriptText appendString:@"	else\n"];
			[scriptText appendString:@"		set window_mode to -1\n"];
			[scriptText appendString:@"	end if\n"];
			[scriptText appendString:@"end tell\n"];
			[scriptText appendString:@"return window_mode\n"];
			
			NSAppleScript *script = [[NSAppleScript alloc] initWithSource:scriptText];
			NSAppleEventDescriptor *result = [script executeAndReturnError:&error];
			NSData *data = [result data];
			long topWindowMode = 0;
			[data getBytes:&topWindowMode length:[data length]];
			
			if (topWindowMode == 0) {
				NSLog(@"Detected opening of an Incognito window");
			}
		}
		
		_childProcs = [NSArray arrayWithArray:newChildProcs];
	}
}
@end
